package preguntas;

public class TestDepresionDatos {
    
    public int cantidadPreguntas;
    public int respuestasPorPregunta;
    public String[] preguntas;
    public String[][] respuestas;
    public String[] resultados;
    
    public TestDepresionDatos(){
        
        cantidadPreguntas = 21;
        respuestasPorPregunta = 7;
        preguntas = new String[cantidadPreguntas];
        respuestas = new String[cantidadPreguntas][respuestasPorPregunta];
        resultados = new String[4];
        
        //Llenar preguntas
        preguntas[0] = "<html>1. Tristeza</html>";
        preguntas[1] = "<html>2. Pesimismo</html>";
        preguntas[2] = "<html>3. Fracaso</html>";
        preguntas[3] = "<html>4. Pérdida de Placer</html>";
        preguntas[4] = "<html>5. Sentimientos de Culpa</html>";
        preguntas[5] = "<html>6. Sentimientos de Castigo</html>";
        preguntas[6] = "<html>7. Disconformidad con uno mismo</html>";
        preguntas[7] = "<html>8. Autocrítica</html>";
        preguntas[8] = "<html>9. Pensamientos o Deseos Suicidas</html>";
        preguntas[9] = "<html>10. Llanto</html>";
        preguntas[10] = "<html>11. Agitación</html>";
        preguntas[11] = "<html>12. Pérdida de Interés</html>";
        preguntas[12] = "<html>13. Indecisión</html>";
        preguntas[13] = "<html>14. Desvalorización</html>";
        preguntas[14] = "<html>15. Pérdida de Energía</html>";
        preguntas[15] = "<html>16. Cambios en los Hábitos de Sueño</html>";
        preguntas[16] = "<html>17. Irritabilidad</html>";
        preguntas[17] = "<html>18. Cambios en el Apetito</html>";
        preguntas[18] = "<html>19. Dificultad de Concentración</html>";
        preguntas[19] = "<html>20. Cansancio o Fatiga</html>";
        preguntas[20] = "<html>21. Pérdida de Interés en el Sexo</html>";
        
        //Llenar respuestas;
        respuestas[0][0] = "<html>No me siento triste</html>";
        respuestas[0][1] = "<html>Me siento triste gran parte del tiempo</html>";
        respuestas[0][2] = "<html>Me siento triste todo el tiempo</html>";
        respuestas[0][3] = "<html>Me siento tan triste o soy tan infeliz que no puedo   soportarlo</html>";
        
        respuestas[1][0] = "<html>No estoy desalentado respecto del mi futuro</html>";
        respuestas[1][1] = "<html>Me siento más desalentado respecto de mi futuro que lo que solía estarlo</html>";
        respuestas[1][2] = "<html>No espero que las cosas funcionen para mi</html>";
        respuestas[1][3] = "<html>Siento que no hay esperanza para mi futuro y que sólo puede empeorar</html>";
        
        respuestas[2][0] = "<html>No me siento como un fracasado</html>";
        respuestas[2][1] = "<html>He fracasado más de lo que hubiera debido</html>";
        respuestas[2][2] = "<html>Cuando miro hacia atrás, veo muchos fracasos</html>";
        respuestas[2][3] = "<html>Siento que como persona soy un fracaso total</html>";
        
        respuestas[3][0] = "<html>Obtengo tanto placer como siempre por las cosas de las que disfruto</html>";
        respuestas[3][1] = "<html>No disfruto tanto de las cosas como solía hacerlo</html>";
        respuestas[3][2] = "<html>Obtengo muy poco placer de las cosas que solía disfrutar</html>";
        respuestas[3][3] = "<html>No puedo obtener ningún placer de las cosas de las que solía disfrutar</html>";
        
        respuestas[4][0] = "<html>No me siento particularmente culpable</html>";
        respuestas[4][1] = "<html>Me siento culpable respecto de varias cosas que he hecho o que debería haber hecho</html>";
        respuestas[4][2] = "<html>Me siento bastante culpable la mayor parte del tiempo</html>";
        respuestas[4][3] = "<html>Me siento culpable todo el tiempo</html>";
        
        respuestas[5][0] = "<html>No siento que este siendo castigado</html>";
        respuestas[5][1] = "<html>Siento que tal vez pueda ser castigado</html>";
        respuestas[5][2] = "<html>Espero ser castigado</html>";
        respuestas[5][3] = "<html>Siento que estoy siendo castigado</html>";
        
        respuestas[6][0] = "<html>Siento acerca de mi lo mismo que siempre</html>";
        respuestas[6][1] = "<html>He perdido la confianza en mí mismo</html>";
        respuestas[6][2] = "<html>Estoy decepcionado conmigo mismo</html>";
        respuestas[6][3] = "<html>No me gusto a mí mismo</html>";
        
        respuestas[7][0] = "<html>No me critico ni me culpo más de lo habitual</html>";
        respuestas[7][1] = "<html>Estoy más crítico conmigo mismo de lo que solía estarlo</html>";
        respuestas[7][2] = "<html>Me critico a mí mismo por todos mis errores</html>";
        respuestas[7][3] = "<html>Me culpo a mí mismo por todo lo malo que sucede</html>";
        
        respuestas[8][0] = "<html>No tengo ningún pensamiento de matarme</html>";
        respuestas[8][1] = "<html>He tenido pensamientos de matarme, pero no lo haría</html>";
        respuestas[8][2] = "<html>Querría matarme</html>";
        respuestas[8][3] = "<html>Me mataría si tuviera la oportunidad de hacerlo</html>";
        
        respuestas[9][0] = "<html>No lloro más de lo que solía hacerlo</html>";
        respuestas[9][1] = "<html>Lloro más de lo que solía hacerlo </html>";
        respuestas[9][2] = "<html>Lloro por cualquier pequeñez</html>";
        respuestas[9][3] = "<html>Siento ganas de llorar pero no puedo</html>";
        
        
        respuestas[10][0] = "<html>No estoy más inquieto o tenso que lo habitual</html>";
        respuestas[10][1] = "<html>Me siento más inquieto o tenso que  lo habitual</html>";
        respuestas[10][2] = "<html>Estoy tan inquieto o agitado que me es difícil quedarme quieto</html>";
        respuestas[10][3] = "<html>Estoy tan inquieto o agitado que tengo que estar siempre en movimiento o  haciendo algo</html>";
        
        respuestas[11][0] = "<html>No he perdido el interés en otras actividades o personas</html>";
        respuestas[11][1] = "<html>Estoy menos interesado que antes en otras personas o cosas</html>";
        respuestas[11][2] = "<html>He perdido casi todo el interés en otras personas o cosas</html>";
        respuestas[11][3] = "<html>Me es difícil interesarme por algo</html>";
        
        respuestas[12][0] = "<html>Tomo mis propias decisiones tan bien como siempre</html>";
        respuestas[12][1] = "<html>Me resulta más difícil que de costumbre tomar decisiones</html>";
        respuestas[12][2] = "<html>Encuentro mucha más dificultad que antes para tomar decisiones</html>";
        respuestas[12][3] = "<html>Tengo problemas para tomar cualquier decisión</html>";
        
        respuestas[13][0] = "<html>No siento que yo no sea valioso</html>";
        respuestas[13][1] = "<html>No me considero a mi mismo tan valioso y útil como solía considerarme</html>";
        respuestas[13][2] = "<html>Me siento menos valioso cuando me comparo con otros</html>";
        respuestas[13][3] = "<html>Siento que no valgo nada</html>";
        
        respuestas[14][0] = "<html>Tengo tanta energía como siempre</html>";
        respuestas[14][1] = "<html>Tengo menos energía que la que solía tener</html>";
        respuestas[14][2] = "<html>No tengo suficiente energía para hacer demasiado</html>";
        respuestas[14][3] = "<html>No tengo energía suficiente para hacer nada</html>";
        
        respuestas[15][0] = "<html>No he experimentado ningún cambio en mis hábitos de sueño</html>";
        respuestas[15][1] = "<html>Duermo un poco más que lo habitual</html>";
        respuestas[15][2] = "<html>Duermo un poco menos que lo habitual</html>";
        respuestas[15][3] = "<html>Duermo mucho más que lo habitual</html>";
        respuestas[15][4] = "<html>Duermo mucho menos que lo habitual</html>";
        respuestas[15][5] = "<html>Duermo la mayor parte del día</html>";
        respuestas[15][6] = "<html>Me despierto  1-2 horas más temprano y no puedo volver a dormirme</html>";
        
        respuestas[16][0] = "<html>No estoy tan irritable que lo habitual</html>";
        respuestas[16][1] = "<html>Estoy más irritable que lo habitual</html>";
        respuestas[16][2] = "<html>Estoy mucho más irritable que lo habitual</html>";
        respuestas[16][3] = "<html>Estoy irritable todo el tiempo</html>";
        
        respuestas[17][0] = "<html>No he experimentado ningún cambio en mi apetito</html>";
        respuestas[17][1] = "<html>Mi apetito es un poco menor que lo habitual</html>";
        respuestas[17][2] = "<html>Mi apetito es un poco mayor que lo habitual</html>";
        respuestas[17][3] = "<html>Mi apetito es mucho menor que antes</html>";
        respuestas[17][4] = "<html>Mi apetito es mucho mayor que lo habitual</html>";
        respuestas[17][5] = "<html>No tengo apetito en absoluto</html>";
        respuestas[17][6] = "<html>Quiero comer todo el día</html>";
        
        respuestas[18][0] = "<html>Puedo concentrarme tan bien como siempre</html>";
        respuestas[18][1] = "<html>No puedo concentrarme tan bien como habitualmente</html>";
        respuestas[18][2] = "<html>Me es difícil mantener la mente en algo por mucho tiempo</html>";
        respuestas[18][3] = "<html>Encuentro que no puedo concentrarme en nada</html>";
        
        respuestas[19][0] = "<html>No estoy más cansado o fatigado que lo habitual</html>";
        respuestas[19][1] = "<html>Me fatigo o me canso más fácilmente que lo habitual</html>";
        respuestas[19][2] = "<html>Estoy demasiado fatigado o cansado para hacer muchas de las cosas que solía hacer</html>";
        respuestas[19][3] = "<html>Estoy  demasiado fatigado o cansado para hacer la mayoría de las cosas que solía</html>";
        
        respuestas[20][0] = "<html>No he notado ningún cambio reciente en mi interés por el sexo</html>";
        respuestas[20][1] = "<html>Estoy menos interesado en el sexo de lo que solía estarlo</html>";
        respuestas[20][2] = "<html>Estoy mucho menos interesado en el sexo</html>";
        respuestas[20][3] = "<html>He perdido completamente el interés en el sexo</html>";
        
        //Llenar resultados
        resultados[0] = "<html>Usted no tiene depresión.</html>";
        resultados[1] = "<html>Usted cuenta con depresión leve.</html>";
        resultados[2] = "<html>Usted cuenta con depresión moderada.</html>";
        resultados[3] = "<html>Usted cuenta con depresión severa.</html>";
    }
}
