package preguntas;

public class TestEstresDatos {
    
    public int cantidadPreguntas;
    public int respuestasPorPregunta;
    public String[] preguntas;
    public String[][] respuestas;
    public String[] resultados;
    
    public TestEstresDatos(){
        
        cantidadPreguntas = 21;
        respuestasPorPregunta = 2;
        preguntas = new String[cantidadPreguntas];
        respuestas = new String[cantidadPreguntas][respuestasPorPregunta];
        resultados = new String[3];
        
        //Llenar preguntas
        preguntas[0] = "<html>1. Tendencia a sufrir frecuentes dolores de cabeza</html>";
        preguntas[1] = "<html>2. Sensación de estar constantemente en estado de tensión y de no hallarse nunca relajado</html>";
        preguntas[2] = "<html>3. Estar excesivamente cansado la mayor parle del tiempo; no sentirse lo suficientemente descansado y fresco después de dormir</html>";
        preguntas[3] = "<html>4. Sensaciones de presión en la cabeza, como si se tuvieran gomas muy tensas alrededor de ella</html>";
        preguntas[4] = "<html>5. Sensación de falta de energía e impulso; necesidad de todas las reservas de energía para realizar las tareas ordinarias</html>";
        preguntas[5] = "<html>6. Temblores, excesivo sudor, taquicardia</html>";
        preguntas[6] = "<html>7. Problemas de sueño, pesadillas, sueño sin descansar</html>";
        preguntas[7] = "<html>8. Sensación de ahogo y tensión sin razón para ello</html>";
        preguntas[8] = "<html>9. Llegar a la conclusión de que las situaciones nos superan demasiado fácilmente; hacer una montaña de un grano de arena</html>";
        preguntas[9] = "<html>10. Darse cuenta de que los propios sentimientos se hieren con facilidad; ser excesivamente sensible</html>";
        preguntas[10] = "<html>11. Encontrar siempre algo por lo que preocuparse</html>";
        preguntas[11] = "<html>12. Sentarse para tener un momento de relax y pensar en aspectos negativos del pasado y el futuro</html>";
        preguntas[12] = "<html>13. Ser bastante consciente de los procesos del propio organismo; tales como latidos violentos del corazón, pinchazos, etc.</html>";
        preguntas[13] = "<html>14. Reaccionar en exceso ante pequeños problemas diarios, tanto en casa como en el trabajo</html>";
        preguntas[14] = "<html>15. Creer que sucederá lo peor, aún cuando el riesgo es muy pequeño; por ejemplo, no sentirse tranquilo hasta que toda la familia se encuentra, segura, en casa…</html>";
        preguntas[15] = "<html>16. Querer llamar a la oficina durante las vacaciones para asegurarse de que todo va bien</html>";
        preguntas[16] = "<html>17. Tomarse a nivel personal todo aquello que sale mal</html>";
        preguntas[17] = "<html>18. Experimentar sobresaltos cuando suena el teléfono o se produce algún pequeño ruido extraño</html>";
        preguntas[18] = "<html>19. No ser capaz de concentrarse, encasa o en el trabajo; distraerse fácilmente por problemas irrelevantes y poco deseados</html>";
        preguntas[19] = "<html>20. Encontrarse muy indeciso; emplear demasiado tiempo para tomar decisiones, dejando a un lado cosas que tienen que hacerse</html>";
        preguntas[20] = "<html>21. Sentir que se está perdiendo el control sobre muchas situaciones de la vida propia; que uno es víctima desvalida de las circunstancias</html>";
        
        //Llenar respuestas;
        for(int i = 0; i < cantidadPreguntas; i++){
            for(int j = 0; j < respuestasPorPregunta; j++){
                if(j == 0)
                    respuestas[i][j] = "SI";
                else
                    respuestas[i][j] = "NO";
            }
        }
        
        //Llenar resultados
        resultados[0] = "<html>Posees una personalidad estable y muy poco vulnerable al estrés. Aunque las circunstancias sean desfavorables y la vida te de la espalda en alguna ocasión, sabes salir adelante y emerger de tus cenizas como el ave Fénix, y es que crees que la vida ya es un regalo, por eso agradeces lo que tienes en vez de quejarte de lo que no tienes. Tu fuerte y positivo carácter hace que la gente de tu alrededor te aprecie y te pida consejo, lo cual puedes hacer muy bien porque sueles pensar en positivo, ofreciendo alternativas y esperanzas a quienes no siempre la tienen.</html>";
        resultados[1] = "<html>Aunque eres bastante resistente a los contratiempos de la vida, reconoces que en muchas ocasiones te faltan o te han faltado las fuerzas para superar los baches. Tienes potencial para hacerlo, no desfallezcas y cree un poco más en ti mismo, así podrás estar más seguro de tus actos y hacer frente a los acontecimientos con serenidad y madurez. Evita también que te vengan a la mente pensamientos catastrofistas y negativos, cuando éstos aparezcan prueba a negarlos y cambiarlos por pensamientos agradables, como algún recuerdo bonito, un viaje, piensa en alguien que aprecias y te hace sentir bien, etc. Así empezarás el camino hacia pensamientos positivos y a una mayor felicidad.</html>";
        resultados[2] = "<html>Eres una persona extremadamente vulnerable a los contratiempos de la vida, quizás por tu propia personalidad ansiosa, quizás porque la vida no se ha portado bien contigo. Debes hacer frente a esta situación, y aunque parezca un tópico, has de intentar mirar más a la “botella medio llena” y no tanto la “botella medio vacía”. En todas las situaciones o acontecimientos existe algo malo y algo bueno, céntrate más en encontrar lo segundo, aunque te parezca que es imposible, haz un esfuerzo, pues este te será recompensado anímicamente. La vida es un don del que debemos disfrutar y estar agradecidos. Si algo no te gusta, cámbialo, busca alternativas, siempre hay alguna.</html>";
    }
}
