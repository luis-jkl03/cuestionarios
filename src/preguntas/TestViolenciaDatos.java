package preguntas;

public class TestViolenciaDatos {
    
    public int cantidadPreguntas;
    public int respuestasPorPregunta;
    public String[] preguntas;
    public String[][] respuestas;
    public String[] resultados;
    
    public TestViolenciaDatos(){
        
        cantidadPreguntas = 20;
        respuestasPorPregunta = 3;
        preguntas = new String[cantidadPreguntas];
        respuestas = new String[cantidadPreguntas][respuestasPorPregunta];
        resultados = new String[5];
        
        //Llenar preguntas
        preguntas[0] = "<html>1. ¿Cuando se dirige a ti te llama por un apodo que te desagrada y/o con groserías?</html>";
        preguntas[1] = "<html>2. ¿Te ha dicho que andas con alguien más, que tus amigos quieren andar contigo?</html>";
        preguntas[2] = "<html>3. ¿Te dice que tiene otras chavas, te compara con sus ex novias?</html>";
        preguntas[3] = "<html>4. ¿Todo el tiempo quiere saber qué haces y con quién estás?</html>";
        preguntas[4] = "<html>5. ¿Te critica, se burla de tu cuerpo y exagera tus defectos en público o en privado?</html>";
        preguntas[5] = "<html>6. Cuando estás con él, ¿te sientes tensa y sientes que hagas lo que hagas, él se molestará?</html>";
        preguntas[6] = "<html>7. Para decidir lo qué harán cuando salen, ¿Ignora tu opinión?</html>";
        preguntas[7] = "<html>8. Cuando platican, ¿te sientes mal porque sólo te habla de sexo, te pregunta si tuviste relaciones sexuales con tus ex novios?</html>";
        preguntas[8] = "<html>9. ¿Te ha dado algún regalo a cambio de algo que te ofenda o te haya hecho sentir mal?</html>";
        preguntas[9] = "<html>10. Si has cedido a sus deseos sexuales, ¿sientes que ha sido por temor o presión?</html>";
        preguntas[10] = "<html>11. Si tienen relaciones sexuales, ¿te impide o condiciona el uso de métodos anticonceptivos?</html>";
        preguntas[11] = "<html>12. ¿Te ha obligado a ver pornografía y/o a tener prácticas sexuales que te desagraden?</html>";
        preguntas[12] = "<html>13. ¿Te ha presionado u obligado a consumir droga?</html>";
        preguntas[13] = "<html>14. Si toma alcohol o se droga, ¿se comporta violento contigo o con otras personas?</html>";
        preguntas[14] = "<html>15. A causa de los problemas con tu novio, ¿has tenido una o más de las siguientes alteraciones: pérdida de apetito y/o el sueño, malas calificaciones, abandonar la escuela, alejarte de tus amigos (as)?</html>";
        preguntas[15] = "<html>16. Cuando se enojan o discuten, ¿has sentido que tu vida está en peligro?</html>";
        preguntas[16] = "<html>17. ¿Te ha golpeado con alguna parte de su cuerpo o con un objeto?</html>";
        preguntas[17] = "<html>18. ¿Alguna vez te ha causado lesiones que ameriten recibir atención médica, psicológica, jurídica y/o auxilio policial?</html>";
        preguntas[18] = "<html>19. ¿Te ha amenazado con matarse o matarte cuando se enojan o le has dicho que quieres terminar?</html>";
        preguntas[19] = "<html>20. Después de una discusión fuerte, ¿él se muestra cariñoso y atento, te regala cosas y te promete que nunca más volverá a suceder y que “todo cambiará”?</html>";
        
        //Llenar respuestas;
        for(int i = 0; i < cantidadPreguntas; i++){
            for(int j = 0; j < respuestasPorPregunta; j++){
                if(j == 0)
                    respuestas[i][j] = "FRECUENTEMENTE";
                else if(j == 1)
                    respuestas[i][j] = "A VECES";
                else 
                    respuestas[i][j] = "NUNCA";
            }
        }
        
        //Llenar resultados
        resultados[0] = "<html>El noviazgo es una etapa en la que aprendemos a relacionarnos en pareja con la persona que queremos. Toda pareja tiene problemas, pero no todas saben resolverlos de manera sensata, teniendo presente el respeto por las diferencias y los derechos de cada uno.</html>";
        resultados[1] = "<html>Existencia de problemas, pero que se resuelven sin violencia física. Los actos violentos son minimizados y justificados por problemas ajenos a la pareja. Es importante que desarrolles habilidades para resolverlos.</html>";
        resultados[2] = "<html>Tu pareja está usando cada vez más la violencia para resolver los conflictos, y la tensión se empieza a acumular. Crees que puedes controlar la situación y que él cambiará. Es una situación de cuidado, y una señal de que la violencia puede aumentar en el futuro.</html>";
        resultados[3] = "<html>Definitivamente tu relación de noviazgo es violenta, los actos violentos se dan bajo cualquier pretexto y cada vez son más frecuentes e intensos. Después de la agresión, intenta remediar el daño, te pide perdón y te promete que no volverá a ocurrir. Esta es la etapa más difícil porque sientes miedo y vergüenza por lo que pasas. Tienes esperanza que cambiará. Busca ayuda.</html>";
        resultados[4] = "<html>Es urgente que te pongas a salvo, que tomes medidas de seguridad y que recibas inmediatamente ayuda especializada. Tu vida está en peligro, tu salud física y/o mental puede quedar severamente dañada.</html>";
        
    }
}
