package TestResultados;

import Test.MenuPrincipal;
import Test.TestViolencia;
import preguntas.TestViolenciaDatos;

public class TestViolenciaResultados extends javax.swing.JFrame {

    public TestViolenciaResultados(char[] respuestas) {
        initComponents();
        setLocationRelativeTo(null);
        
        TestViolenciaDatos test = new TestViolenciaDatos();
        int respuestasFrecuentemente = 0;
        int respuestasAveces = 0;
        int respuestasNunca = 0;
        int puntos = 0;
        
        
        for(int i = 0; i < respuestas.length; i++){
            if(respuestas[i] == 'A'){
                respuestasFrecuentemente++;
                
                if(i <= 8)
                    puntos = puntos + 2;
                else
                    puntos = puntos + 5;
            }
            else if(respuestas[i] == 'B'){
                respuestasAveces++;
                
                if(i <= 8)
                    puntos = puntos + 1;
                else
                    puntos = puntos + 3;
            }
            else{
                respuestasNunca++;
            }
        }
        
        lbPuntos.setText(puntos + " puntos");
        lbRespuestasFrecuentemente.setText("Frecuentemente - " + respuestasFrecuentemente);
        lbRespuestasAveces.setText("A veces - " + respuestasAveces);
        lbRespuestasNunca.setText("Nunca - " + respuestasNunca);
        
        if(puntos >= 0 && puntos <= 5){
            lbPuntuacionResultado.setText("**Hasta 5 puntos / Relación que no presenta violencia**");
            lbResultado.setText(test.resultados[0]);
        }else if(puntos >= 6 && puntos <= 15){
            lbPuntuacionResultado.setText("**6 a 15 Puntos / Relación con primeras señales de violencia.**");
            lbResultado.setText(test.resultados[1]);
        }else if(puntos >= 16 && puntos <= 25){
            lbPuntuacionResultado.setText("**16 a 25 puntos / Relación de abuso.**");
            lbResultado.setText(test.resultados[2]);
        }else if(puntos >= 26 && puntos <= 40){
            lbPuntuacionResultado.setText("**26 a 40 puntos / Relación de abuso severo.**");
            lbResultado.setText(test.resultados[3]);
        }else if(puntos >= 41){
            lbPuntuacionResultado.setText("**Más de 41 puntos / Relación violenta**");
            lbResultado.setText(test.resultados[4]);
        }
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel = new javax.swing.JPanel();
        lbRespuestasFrecuentemente = new javax.swing.JLabel();
        lbRespuestasAveces = new javax.swing.JLabel();
        lbRespuestasNunca = new javax.swing.JLabel();
        lbTitulo = new javax.swing.JLabel();
        lbIntPuntos = new javax.swing.JLabel();
        lbPuntuacion = new javax.swing.JLabel();
        lbPuntos = new javax.swing.JLabel();
        lbPuntuacionResultado = new javax.swing.JLabel();
        lbResultado = new javax.swing.JLabel();
        barra = new javax.swing.JMenuBar();
        menuArchivo = new javax.swing.JMenu();
        menuItemNuevo = new javax.swing.JMenuItem();
        menuItemMenu = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(700, 650));
        setResizable(false);

        panel.setBackground(new java.awt.Color(204, 204, 255));
        panel.setMinimumSize(new java.awt.Dimension(700, 650));
        panel.setPreferredSize(new java.awt.Dimension(700, 500));

        lbRespuestasFrecuentemente.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        lbRespuestasAveces.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        lbRespuestasNunca.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        lbTitulo.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lbTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTitulo.setText("<html>RESULTADOS TEST DE VIOLENCIA EN EL NOVIAZGO</html>");

        lbIntPuntos.setFont(new java.awt.Font("Times New Roman", 3, 24)); // NOI18N
        lbIntPuntos.setText("Interpretación de puntos");

        lbPuntuacion.setFont(new java.awt.Font("Times New Roman", 2, 24)); // NOI18N
        lbPuntuacion.setText("Puntuación:");

        lbPuntos.setFont(new java.awt.Font("Times New Roman", 3, 24)); // NOI18N

        lbPuntuacionResultado.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbPuntuacionResultado.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lbResultado.setFont(new java.awt.Font("Century", 2, 20)); // NOI18N
        lbResultado.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panelLayout.createSequentialGroup()
                                .addComponent(lbPuntuacion, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lbPuntos, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(67, 67, 67)
                                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbRespuestasAveces, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lbRespuestasFrecuentemente, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lbRespuestasNunca, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(panelLayout.createSequentialGroup()
                                .addComponent(lbIntPuntos, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(241, 241, 241))))
                    .addGroup(panelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lbTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, 680, Short.MAX_VALUE))
                    .addComponent(lbPuntuacionResultado, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lbResultado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbPuntuacion, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbPuntos, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(lbRespuestasFrecuentemente)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbRespuestasAveces)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbRespuestasNunca)
                .addGap(26, 26, 26)
                .addComponent(lbIntPuntos, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(lbPuntuacionResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lbResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
        );

        getContentPane().add(panel, java.awt.BorderLayout.CENTER);

        menuArchivo.setText("Archivo");

        menuItemNuevo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        menuItemNuevo.setText("Comenzar test de nuevo");
        menuItemNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemNuevoActionPerformed(evt);
            }
        });
        menuArchivo.add(menuItemNuevo);

        menuItemMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        menuItemMenu.setText("Regresar al menú");
        menuItemMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemMenuActionPerformed(evt);
            }
        });
        menuArchivo.add(menuItemMenu);

        barra.add(menuArchivo);

        setJMenuBar(barra);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void menuItemNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemNuevoActionPerformed
        this.dispose();
        new TestViolencia().setVisible(true);
    }//GEN-LAST:event_menuItemNuevoActionPerformed

    private void menuItemMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemMenuActionPerformed
        this.dispose();
        new MenuPrincipal().setVisible(true);
    }//GEN-LAST:event_menuItemMenuActionPerformed

    
       public static void main(String[] args){
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TestViolenciaResultados(new char[21]).setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar barra;
    private javax.swing.JLabel lbIntPuntos;
    private javax.swing.JLabel lbPuntos;
    private javax.swing.JLabel lbPuntuacion;
    private javax.swing.JLabel lbPuntuacionResultado;
    private javax.swing.JLabel lbRespuestasAveces;
    private javax.swing.JLabel lbRespuestasFrecuentemente;
    private javax.swing.JLabel lbRespuestasNunca;
    private javax.swing.JLabel lbResultado;
    private javax.swing.JLabel lbTitulo;
    private javax.swing.JMenu menuArchivo;
    private javax.swing.JMenuItem menuItemMenu;
    private javax.swing.JMenuItem menuItemNuevo;
    private javax.swing.JPanel panel;
    // End of variables declaration//GEN-END:variables
}
