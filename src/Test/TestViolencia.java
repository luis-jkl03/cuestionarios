package Test;

import Acciones.Acciones;
import TestResultados.TestViolenciaResultados;
import javax.swing.ImageIcon;
import preguntas.TestViolenciaDatos;

public class TestViolencia extends javax.swing.JFrame {
    
    private TestViolenciaDatos test;
    private Acciones acciones;
    private int indicePregunta;
    private char[] respuestas;

    public TestViolencia() {
        initComponents();
        setLocationRelativeTo(null);
        
        test = new TestViolenciaDatos();
        acciones = new Acciones();
        indicePregunta = 0;
        respuestas = new char[test.cantidadPreguntas];
        
        //Agregar primer pregunta
        lbPregunta.setText(test.preguntas[indicePregunta]);
        opcionA.setText(test.respuestas[indicePregunta][0]);
        opcionB.setText(test.respuestas[indicePregunta][1]);
        opcionC.setText(test.respuestas[indicePregunta][2]);
        
        btnAnterior.setVisible(false);
        
        imagenBotonSiguiente();
        imagenBotonAnterior();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGrupo = new javax.swing.ButtonGroup();
        panel = new javax.swing.JPanel();
        opcionA = new javax.swing.JRadioButton();
        opcionB = new javax.swing.JRadioButton();
        lbTitulo = new javax.swing.JLabel();
        lbPregunta = new javax.swing.JLabel();
        btnSiguiente = new javax.swing.JButton();
        btnAnterior = new javax.swing.JButton();
        lbProgreso = new javax.swing.JLabel();
        opcionC = new javax.swing.JRadioButton();
        barra = new javax.swing.JMenuBar();
        menuArchivo = new javax.swing.JMenu();
        menuItemNuevo = new javax.swing.JMenuItem();
        menuItemMenu = new javax.swing.JMenuItem();
        menuOpciones = new javax.swing.JMenu();
        menuItemFrecuentemente = new javax.swing.JMenuItem();
        menuItemAveces = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        menuItemSiguiente = new javax.swing.JMenuItem();
        menuItemAnterior = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Test de Estres");
        setResizable(false);

        panel.setBackground(new java.awt.Color(255, 153, 102));
        panel.setMinimumSize(new java.awt.Dimension(700, 500));
        panel.setPreferredSize(new java.awt.Dimension(700, 500));

        btnGrupo.add(opcionA);
        opcionA.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        opcionA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcionAActionPerformed(evt);
            }
        });

        btnGrupo.add(opcionB);
        opcionB.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        opcionB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcionBActionPerformed(evt);
            }
        });

        lbTitulo.setFont(new java.awt.Font("Verdana", 1, 36)); // NOI18N
        lbTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTitulo.setText("Test de Violencia en el Noviazgo");

        lbPregunta.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lbPregunta.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        btnSiguiente.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSiguiente.setContentAreaFilled(false);
        btnSiguiente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSiguiente.setEnabled(false);
        btnSiguiente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSiguienteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSiguienteMouseExited(evt);
            }
        });
        btnSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteActionPerformed(evt);
            }
        });

        btnAnterior.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnAnterior.setContentAreaFilled(false);
        btnAnterior.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAnterior.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAnteriorMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAnteriorMouseExited(evt);
            }
        });
        btnAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnteriorActionPerformed(evt);
            }
        });

        lbProgreso.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbProgreso.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbProgreso.setText("Completado - 0%");

        btnGrupo.add(opcionC);
        opcionC.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        opcionC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcionCActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 674, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(panelLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(opcionC)
                            .addComponent(opcionA)
                            .addComponent(opcionB))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(panelLayout.createSequentialGroup()
                        .addComponent(btnAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lbProgreso, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(115, 115, 115)
                        .addComponent(btnSiguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24))
                    .addGroup(panelLayout.createSequentialGroup()
                        .addComponent(lbPregunta, javax.swing.GroupLayout.PREFERRED_SIZE, 639, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(27, Short.MAX_VALUE))))
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(lbTitulo)
                .addGap(28, 28, 28)
                .addComponent(lbPregunta, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(opcionA)
                .addGap(18, 18, 18)
                .addComponent(opcionB)
                .addGap(18, 18, 18)
                .addComponent(opcionC)
                .addGap(42, 42, 42)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSiguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbProgreso, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46))
        );

        getContentPane().add(panel, java.awt.BorderLayout.CENTER);

        menuArchivo.setText("Archivo");

        menuItemNuevo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_MASK));
        menuItemNuevo.setText("Comenzar de nuevo");
        menuItemNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemNuevoActionPerformed(evt);
            }
        });
        menuArchivo.add(menuItemNuevo);

        menuItemMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        menuItemMenu.setText("Volver al menu");
        menuItemMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemMenuActionPerformed(evt);
            }
        });
        menuArchivo.add(menuItemMenu);

        barra.add(menuArchivo);

        menuOpciones.setText("Opciones");

        menuItemFrecuentemente.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_MASK));
        menuItemFrecuentemente.setText("Seleccionar opción frecuentemente");
        menuItemFrecuentemente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemFrecuentementeActionPerformed(evt);
            }
        });
        menuOpciones.add(menuItemFrecuentemente);

        menuItemAveces.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        menuItemAveces.setText("Seleccionar opción A veces");
        menuItemAveces.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemAvecesActionPerformed(evt);
            }
        });
        menuOpciones.add(menuItemAveces);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Seleccionar opción Nunca");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        menuOpciones.add(jMenuItem1);

        menuItemSiguiente.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_RIGHT, java.awt.event.InputEvent.CTRL_MASK));
        menuItemSiguiente.setText("Siguiente pregunta");
        menuItemSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemSiguienteActionPerformed(evt);
            }
        });
        menuOpciones.add(menuItemSiguiente);

        menuItemAnterior.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_LEFT, java.awt.event.InputEvent.CTRL_MASK));
        menuItemAnterior.setText("Anterior pregunta");
        menuItemAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemAnteriorActionPerformed(evt);
            }
        });
        menuOpciones.add(menuItemAnterior);

        barra.add(menuOpciones);

        setJMenuBar(barra);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteActionPerformed
        siguientePregunta();
    }//GEN-LAST:event_btnSiguienteActionPerformed

    private void btnAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnteriorActionPerformed
        anteriorPregunta();
    }//GEN-LAST:event_btnAnteriorActionPerformed

    private void menuItemNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemNuevoActionPerformed
        indicePregunta = 0;
        respuestas = new char[test.cantidadPreguntas];
        btnGrupo.clearSelection();
        
        //Agregar primer pregunta
        lbPregunta.setText(test.preguntas[indicePregunta]);
        opcionA.setText(test.respuestas[indicePregunta][0]);
        opcionB.setText(test.respuestas[indicePregunta][1]);
        opcionC.setText(test.respuestas[indicePregunta][2]);
        
        lbProgreso.setText("Completado - 0%");
        btnAnterior.setVisible(false);
        btnSiguiente.setEnabled(false);
        imagenBotonSiguiente();
    }//GEN-LAST:event_menuItemNuevoActionPerformed

    private void menuItemMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemMenuActionPerformed
        this.dispose();
        new MenuPrincipal().setVisible(true);
    }//GEN-LAST:event_menuItemMenuActionPerformed

    private void menuItemFrecuentementeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemFrecuentementeActionPerformed
        opcionA.setSelected(true);
        opcionSeleccionada();
    }//GEN-LAST:event_menuItemFrecuentementeActionPerformed

    private void menuItemAvecesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemAvecesActionPerformed
        opcionB.setSelected(true);
        opcionSeleccionada();
    }//GEN-LAST:event_menuItemAvecesActionPerformed

    private void menuItemSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemSiguienteActionPerformed
        if(respuestas[indicePregunta] != 0){
            siguientePregunta();
        }else{
            acciones.dialogoSeleccionarOpcion(this);
        }
    }//GEN-LAST:event_menuItemSiguienteActionPerformed

    private void menuItemAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemAnteriorActionPerformed
        if(indicePregunta > 0)
            anteriorPregunta();
    }//GEN-LAST:event_menuItemAnteriorActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        opcionC.setSelected(true);
        opcionSeleccionada();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void opcionAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcionAActionPerformed
        opcionSeleccionada();
    }//GEN-LAST:event_opcionAActionPerformed

    private void opcionBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcionBActionPerformed
        opcionSeleccionada();
    }//GEN-LAST:event_opcionBActionPerformed

    private void opcionCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcionCActionPerformed
        opcionSeleccionada();
    }//GEN-LAST:event_opcionCActionPerformed

    private void btnSiguienteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSiguienteMouseEntered
        if(indicePregunta == test.cantidadPreguntas - 1)
            imagenBotonFinalizarGrande();
        else
            imagenBotonSiguienteGrande();
    }//GEN-LAST:event_btnSiguienteMouseEntered

    private void btnSiguienteMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSiguienteMouseExited
        if(indicePregunta == test.cantidadPreguntas - 1)
            imagenBotonFinalizar();
        else
            imagenBotonSiguiente();
    }//GEN-LAST:event_btnSiguienteMouseExited

    private void btnAnteriorMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAnteriorMouseEntered
        imagenBotonAnteriorGrande();
    }//GEN-LAST:event_btnAnteriorMouseEntered

    private void btnAnteriorMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAnteriorMouseExited
        imagenBotonAnterior();
    }//GEN-LAST:event_btnAnteriorMouseExited

    private void siguientePregunta(){
                
        if(indicePregunta == test.cantidadPreguntas - 1){
            int respuesta = acciones.dialogoTerminarTest(this);
            if(respuesta == 0){
                this.dispose();
                new TestViolenciaResultados(respuestas).setVisible(true);
            }
        }else{
            indicePregunta++;
            lbPregunta.setText(test.preguntas[indicePregunta]);
            opcionA.setText(test.respuestas[indicePregunta][0]);
            opcionB.setText(test.respuestas[indicePregunta][1]);
            opcionC.setText(test.respuestas[indicePregunta][2]);

            llenarRespuesta();

            if(indicePregunta == test.cantidadPreguntas - 1)
                imagenBotonFinalizar();
        }

        if(indicePregunta > 0){
            btnAnterior.setVisible(true);
        }
    }
    
    private void anteriorPregunta(){
        
        indicePregunta--;
        llenarRespuesta();
        
        if(indicePregunta == 0){
            btnAnterior.setVisible(false);
        }
        
        if(indicePregunta < test.cantidadPreguntas - 1){
            imagenBotonSiguiente();
        }
        
        lbPregunta.setText(test.preguntas[indicePregunta]);
        opcionA.setText(test.respuestas[indicePregunta][0]);
        opcionB.setText(test.respuestas[indicePregunta][1]);
        opcionC.setText(test.respuestas[indicePregunta][2]);
    }
    
    private void opcionSeleccionada(){
        btnSiguiente.setEnabled(true);
        guardarRespuesta();
        actualizarProgreso();
    }
    
    private void guardarRespuesta(){
        respuestas[indicePregunta] = acciones.obtenerRespuesta(opcionA, opcionB, opcionC);
    }
    
    private void llenarRespuesta(){
        
        btnGrupo.clearSelection();
        
        char respuesta = respuestas[indicePregunta];
        
        switch(respuesta){
            case 'A':
                opcionA.setSelected(true);
                break;
            case 'B':
                opcionB.setSelected(true);
                break;
            case 'C':
                opcionC.setSelected(true);
                break;
        }
        
        if(respuesta != 0)
            btnSiguiente.setEnabled(true);
        else
            btnSiguiente.setEnabled(false);
    }
    
    private void actualizarProgreso(){
        int contestadas = 0;
        int completado = 0;
        for(int i = 0; i < test.cantidadPreguntas; i++){
            if(respuestas[i] != 0)
                contestadas++;
        }
        
        completado = contestadas * 100 / test.cantidadPreguntas; 
        
        lbProgreso.setText("Completado - " + completado + "%");
    }
    
    private void imagenBotonSiguiente(){
        int ancho = btnSiguiente.getSize().width;
        int alto = btnSiguiente.getSize().height - 10;
        ImageIcon imagenSiguiente = new ImageIcon(acciones.rutaImagenes + "siguiente.png");
        
        btnSiguiente.setIcon(acciones.ajustarImagen(imagenSiguiente, ancho, alto));
    }
    
     private void imagenBotonSiguienteGrande(){
        if(respuestas[indicePregunta] != 0){
            int ancho = btnSiguiente.getSize().width;
            int alto = btnSiguiente.getSize().height + 10;
            ImageIcon imagenSiguiente = new ImageIcon(acciones.rutaImagenes + "siguiente.png");

            btnSiguiente.setIcon(acciones.ajustarImagen(imagenSiguiente, ancho, alto));
        }
    }
    
    private void imagenBotonAnterior(){
        int ancho = btnAnterior.getSize().width;
        int alto = btnAnterior.getSize().height - 10;
        ImageIcon imagenAnterior = new ImageIcon(acciones.rutaImagenes + "anterior.png");
        
        btnAnterior.setIcon(acciones.ajustarImagen(imagenAnterior, ancho, alto));
    }
    
    private void imagenBotonAnteriorGrande(){
        int ancho = btnAnterior.getSize().width;
        int alto = btnAnterior.getSize().height + 10;
        ImageIcon imagenAnterior = new ImageIcon(acciones.rutaImagenes + "anterior.png");
        
        btnAnterior.setIcon(acciones.ajustarImagen(imagenAnterior, ancho, alto));
    }
    
    private void imagenBotonFinalizar(){
        int ancho = btnSiguiente.getSize().width;
        int alto = btnSiguiente.getSize().height - 10;
        ImageIcon imagenFinalizar = new ImageIcon(acciones.rutaImagenes + "finalizar.png");
        
        btnSiguiente.setIcon(acciones.ajustarImagen(imagenFinalizar, ancho, alto));
    }
    
    private void imagenBotonFinalizarGrande(){
        if(respuestas[indicePregunta] != 0){
            int ancho = btnSiguiente.getSize().width;
            int alto = btnSiguiente.getSize().height + 10;
            ImageIcon imagenFinalizar = new ImageIcon(acciones.rutaImagenes + "finalizar.png");

            btnSiguiente.setIcon(acciones.ajustarImagen(imagenFinalizar, ancho, alto));
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar barra;
    private javax.swing.JButton btnAnterior;
    private javax.swing.ButtonGroup btnGrupo;
    private javax.swing.JButton btnSiguiente;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JLabel lbPregunta;
    private javax.swing.JLabel lbProgreso;
    private javax.swing.JLabel lbTitulo;
    private javax.swing.JMenu menuArchivo;
    private javax.swing.JMenuItem menuItemAnterior;
    private javax.swing.JMenuItem menuItemAveces;
    private javax.swing.JMenuItem menuItemFrecuentemente;
    private javax.swing.JMenuItem menuItemMenu;
    private javax.swing.JMenuItem menuItemNuevo;
    private javax.swing.JMenuItem menuItemSiguiente;
    private javax.swing.JMenu menuOpciones;
    private javax.swing.JRadioButton opcionA;
    private javax.swing.JRadioButton opcionB;
    private javax.swing.JRadioButton opcionC;
    private javax.swing.JPanel panel;
    // End of variables declaration//GEN-END:variables
}
