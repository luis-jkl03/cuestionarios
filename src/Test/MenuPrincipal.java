package Test;

public class MenuPrincipal extends javax.swing.JFrame {

    public MenuPrincipal() {
        initComponents();
        setLocationRelativeTo(null);
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel = new javax.swing.JPanel();
        lbTitulo = new javax.swing.JLabel();
        lbTestValores = new javax.swing.JLabel();
        lbTestAutoestima = new javax.swing.JLabel();
        lbTestInteligencia = new javax.swing.JLabel();
        lbTestFortaleza = new javax.swing.JLabel();
        lbTestPersonalidad = new javax.swing.JLabel();
        btnTestInteligencia = new javax.swing.JButton();
        btnTestAutoestima = new javax.swing.JButton();
        btnTestPersonalidad = new javax.swing.JButton();
        btnTestValores = new javax.swing.JButton();
        btnTestFortaleza = new javax.swing.JButton();
        lbTestAutoestimaPorcentaje = new javax.swing.JLabel();
        lbTestInteligenciaPorcentaje = new javax.swing.JLabel();
        lbTestValoresPorcentaje = new javax.swing.JLabel();
        lbTestPersonalidadPorcentaje = new javax.swing.JLabel();
        lbTestFortalezaPorcentaje = new javax.swing.JLabel();
        barra = new javax.swing.JMenuBar();
        menuArchivo = new javax.swing.JMenu();
        menuItemSalir = new javax.swing.JMenuItem();
        menuTest = new javax.swing.JMenu();
        menuItemEstres = new javax.swing.JMenuItem();
        menuItemViolencia = new javax.swing.JMenuItem();
        menuItemDepresion = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Menú de Test");
        setMinimumSize(new java.awt.Dimension(1000, 600));
        setResizable(false);

        panel.setBackground(new java.awt.Color(238, 238, 238));
        panel.setMinimumSize(new java.awt.Dimension(1000, 600));

        lbTitulo.setFont(new java.awt.Font("Verdana", 1, 36)); // NOI18N
        lbTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTitulo.setText("SELECCIONA UN TEST");

        lbTestValores.setFont(new java.awt.Font("Sylfaen", 1, 36)); // NOI18N
        lbTestValores.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbTestValores.setText("3. Test de Depresión");

        lbTestAutoestima.setFont(new java.awt.Font("Sylfaen", 1, 36)); // NOI18N
        lbTestAutoestima.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbTestAutoestima.setText("2. Test de Violencia en el Noviazgo");

        lbTestInteligencia.setBackground(new java.awt.Color(204, 204, 204));
        lbTestInteligencia.setFont(new java.awt.Font("Sylfaen", 1, 36)); // NOI18N
        lbTestInteligencia.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbTestInteligencia.setText("1. Test de Estrés");

        lbTestFortaleza.setFont(new java.awt.Font("Sylfaen", 1, 36)); // NOI18N
        lbTestFortaleza.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbTestFortaleza.setText("5. Test de Fortaleza");

        lbTestPersonalidad.setFont(new java.awt.Font("Sylfaen", 1, 36)); // NOI18N
        lbTestPersonalidad.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbTestPersonalidad.setText("4. Test de Personalidad");

        btnTestInteligencia.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        btnTestInteligencia.setText("INICIAR");
        btnTestInteligencia.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnTestInteligencia.setMinimumSize(new java.awt.Dimension(73, 100));
        btnTestInteligencia.setPreferredSize(new java.awt.Dimension(73, 35));
        btnTestInteligencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestInteligenciaActionPerformed(evt);
            }
        });

        btnTestAutoestima.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        btnTestAutoestima.setText("INICIAR");
        btnTestAutoestima.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnTestAutoestima.setMinimumSize(new java.awt.Dimension(73, 100));
        btnTestAutoestima.setPreferredSize(new java.awt.Dimension(73, 35));
        btnTestAutoestima.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestAutoestimaActionPerformed(evt);
            }
        });

        btnTestPersonalidad.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        btnTestPersonalidad.setText("INICIAR");
        btnTestPersonalidad.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnTestPersonalidad.setMinimumSize(new java.awt.Dimension(73, 100));
        btnTestPersonalidad.setPreferredSize(new java.awt.Dimension(73, 35));
        btnTestPersonalidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestPersonalidadActionPerformed(evt);
            }
        });

        btnTestValores.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        btnTestValores.setText("INICIAR");
        btnTestValores.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnTestValores.setMinimumSize(new java.awt.Dimension(73, 100));
        btnTestValores.setPreferredSize(new java.awt.Dimension(73, 35));
        btnTestValores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestValoresActionPerformed(evt);
            }
        });

        btnTestFortaleza.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        btnTestFortaleza.setText("INICIAR");
        btnTestFortaleza.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnTestFortaleza.setMinimumSize(new java.awt.Dimension(73, 100));
        btnTestFortaleza.setPreferredSize(new java.awt.Dimension(73, 35));
        btnTestFortaleza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestFortalezaActionPerformed(evt);
            }
        });

        lbTestAutoestimaPorcentaje.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lbTestAutoestimaPorcentaje.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbTestAutoestimaPorcentaje.setText("Completado 0%");

        lbTestInteligenciaPorcentaje.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lbTestInteligenciaPorcentaje.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbTestInteligenciaPorcentaje.setText("Completado 0%");

        lbTestValoresPorcentaje.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lbTestValoresPorcentaje.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbTestValoresPorcentaje.setText("Completado 0%");

        lbTestPersonalidadPorcentaje.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lbTestPersonalidadPorcentaje.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbTestPersonalidadPorcentaje.setText("Completado 0%");

        lbTestFortalezaPorcentaje.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lbTestFortalezaPorcentaje.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbTestFortalezaPorcentaje.setText("Completado 0%");

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 447, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(271, 271, 271))
            .addGroup(panelLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addComponent(lbTestFortaleza, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnTestFortaleza, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelLayout.createSequentialGroup()
                                .addComponent(lbTestInteligencia, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnTestInteligencia, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(207, 207, 207)
                                .addComponent(lbTestInteligenciaPorcentaje, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelLayout.createSequentialGroup()
                                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelLayout.createSequentialGroup()
                                        .addComponent(lbTestValores, javax.swing.GroupLayout.PREFERRED_SIZE, 355, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(btnTestValores, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelLayout.createSequentialGroup()
                                        .addComponent(lbTestPersonalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 409, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnTestPersonalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(45, 45, 45)
                                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbTestPersonalidadPorcentaje, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lbTestValoresPorcentaje, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lbTestFortalezaPorcentaje, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelLayout.createSequentialGroup()
                                .addComponent(lbTestAutoestima, javax.swing.GroupLayout.PREFERRED_SIZE, 608, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTestAutoestima, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(lbTestAutoestimaPorcentaje, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 52, Short.MAX_VALUE))))
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addComponent(lbTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(93, 93, 93)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbTestInteligencia, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbTestInteligenciaPorcentaje, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTestInteligencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbTestAutoestima, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTestAutoestima, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbTestAutoestimaPorcentaje, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbTestValores, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnTestValores, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lbTestValoresPorcentaje, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbTestPersonalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbTestPersonalidadPorcentaje, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTestPersonalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnTestFortaleza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbTestFortaleza, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbTestFortalezaPorcentaje, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(110, Short.MAX_VALUE))
        );

        getContentPane().add(panel, java.awt.BorderLayout.CENTER);

        menuArchivo.setText("Archivo");

        menuItemSalir.setText("Salir");
        menuItemSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemSalirActionPerformed(evt);
            }
        });
        menuArchivo.add(menuItemSalir);

        barra.add(menuArchivo);

        menuTest.setText("Test");

        menuItemEstres.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        menuItemEstres.setText("Iniciar Test de Estrés");
        menuItemEstres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemEstresActionPerformed(evt);
            }
        });
        menuTest.add(menuItemEstres);

        menuItemViolencia.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_MASK));
        menuItemViolencia.setText("Iniciar Test de Violencia en el Noviazgo");
        menuItemViolencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemViolenciaActionPerformed(evt);
            }
        });
        menuTest.add(menuItemViolencia);

        menuItemDepresion.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        menuItemDepresion.setText("Iniciar Test de Depresión");
        menuItemDepresion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemDepresionActionPerformed(evt);
            }
        });
        menuTest.add(menuItemDepresion);

        jMenuItem5.setText("jMenuItem5");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        menuTest.add(jMenuItem5);

        jMenuItem6.setText("jMenuItem6");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menuTest.add(jMenuItem6);

        barra.add(menuTest);

        setJMenuBar(barra);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnTestInteligenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestInteligenciaActionPerformed
        this.dispose();
        new TestEstres().setVisible(true);
    }//GEN-LAST:event_btnTestInteligenciaActionPerformed

    private void btnTestAutoestimaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestAutoestimaActionPerformed
        this.dispose();
        new TestViolencia().setVisible(true);
    }//GEN-LAST:event_btnTestAutoestimaActionPerformed

    private void btnTestValoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestValoresActionPerformed
        this.dispose();
        new TestDepresion().setVisible(true);
    }//GEN-LAST:event_btnTestValoresActionPerformed

    private void btnTestPersonalidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestPersonalidadActionPerformed
        this.dispose();;
    }//GEN-LAST:event_btnTestPersonalidadActionPerformed

    private void btnTestFortalezaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestFortalezaActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnTestFortalezaActionPerformed

    private void menuItemEstresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemEstresActionPerformed
        this.dispose();
        new TestEstres().setVisible(true);
    }//GEN-LAST:event_menuItemEstresActionPerformed

    private void menuItemViolenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemViolenciaActionPerformed
        this.dispose();
        new TestViolencia().setVisible(true);
    }//GEN-LAST:event_menuItemViolenciaActionPerformed

    private void menuItemDepresionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemDepresionActionPerformed
        this.dispose();
        new TestDepresion().setVisible(true);
    }//GEN-LAST:event_menuItemDepresionActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void menuItemSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_menuItemSalirActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar barra;
    private javax.swing.JButton btnTestAutoestima;
    private javax.swing.JButton btnTestFortaleza;
    private javax.swing.JButton btnTestInteligencia;
    private javax.swing.JButton btnTestPersonalidad;
    private javax.swing.JButton btnTestValores;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JLabel lbTestAutoestima;
    private javax.swing.JLabel lbTestAutoestimaPorcentaje;
    private javax.swing.JLabel lbTestFortaleza;
    private javax.swing.JLabel lbTestFortalezaPorcentaje;
    private javax.swing.JLabel lbTestInteligencia;
    private javax.swing.JLabel lbTestInteligenciaPorcentaje;
    private javax.swing.JLabel lbTestPersonalidad;
    private javax.swing.JLabel lbTestPersonalidadPorcentaje;
    private javax.swing.JLabel lbTestValores;
    private javax.swing.JLabel lbTestValoresPorcentaje;
    private javax.swing.JLabel lbTitulo;
    private javax.swing.JMenu menuArchivo;
    private javax.swing.JMenuItem menuItemDepresion;
    private javax.swing.JMenuItem menuItemEstres;
    private javax.swing.JMenuItem menuItemSalir;
    private javax.swing.JMenuItem menuItemViolencia;
    private javax.swing.JMenu menuTest;
    private javax.swing.JPanel panel;
    // End of variables declaration//GEN-END:variables
}
