package Test;

import Acciones.Acciones;
import TestResultados.TestDepresionResultados;
import javax.swing.ImageIcon;
import preguntas.TestDepresionDatos;

public class TestDepresion extends javax.swing.JFrame {
    
    private TestDepresionDatos test;
    private Acciones acciones;
    private int indicePregunta;
    private char[] respuestas;

    public TestDepresion() {
        initComponents();
        setLocationRelativeTo(null);
        
        test = new TestDepresionDatos();
        acciones = new Acciones();
        indicePregunta = 0;
        respuestas = new char[test.cantidadPreguntas];
        
        //Agregar primer pregunta
        lbPregunta.setText(test.preguntas[indicePregunta]);
        opcionA.setText(test.respuestas[indicePregunta][0]);
        opcionB.setText(test.respuestas[indicePregunta][1]);
        opcionC.setText(test.respuestas[indicePregunta][2]);
        opcionD.setText(test.respuestas[indicePregunta][3]);
        
        mostrarGrupo2(false);
        btnAnterior.setVisible(false);
        
        imagenBotonSiguiente();
        imagenBotonAnterior();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGrupo1 = new javax.swing.ButtonGroup();
        btnGrupo2 = new javax.swing.ButtonGroup();
        panel = new javax.swing.JPanel();
        opcionA = new javax.swing.JRadioButton();
        opcionB = new javax.swing.JRadioButton();
        opcionC = new javax.swing.JRadioButton();
        opcionD = new javax.swing.JRadioButton();
        opcion0 = new javax.swing.JRadioButton();
        opcion1A = new javax.swing.JRadioButton();
        opcion1B = new javax.swing.JRadioButton();
        opcion2A = new javax.swing.JRadioButton();
        opcion2B = new javax.swing.JRadioButton();
        opcion3A = new javax.swing.JRadioButton();
        opcion3B = new javax.swing.JRadioButton();
        lbTitulo = new javax.swing.JLabel();
        lbPregunta = new javax.swing.JLabel();
        btnSiguiente = new javax.swing.JButton();
        btnAnterior = new javax.swing.JButton();
        lbProgreso = new javax.swing.JLabel();
        barra = new javax.swing.JMenuBar();
        menuArchivo = new javax.swing.JMenu();
        menuItemNuevo = new javax.swing.JMenuItem();
        menuItemMenu = new javax.swing.JMenuItem();
        menuOpciones = new javax.swing.JMenu();
        menuItemRes1 = new javax.swing.JMenuItem();
        menuItemRes2 = new javax.swing.JMenuItem();
        menuItemRes3 = new javax.swing.JMenuItem();
        menuItemRes4 = new javax.swing.JMenuItem();
        menuItemRes5 = new javax.swing.JMenuItem();
        menuItemRes6 = new javax.swing.JMenuItem();
        menuItemRes7 = new javax.swing.JMenuItem();
        menuItemSiguiente = new javax.swing.JMenuItem();
        menuItemAnterior = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Test de Estres");
        setMinimumSize(new java.awt.Dimension(720, 650));
        setResizable(false);

        panel.setBackground(new java.awt.Color(51, 190, 55));
        panel.setMinimumSize(new java.awt.Dimension(720, 650));
        panel.setPreferredSize(new java.awt.Dimension(720, 650));

        btnGrupo1.add(opcionA);
        opcionA.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        opcionA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcionAActionPerformed(evt);
            }
        });

        btnGrupo1.add(opcionB);
        opcionB.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        opcionB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcionBActionPerformed(evt);
            }
        });

        btnGrupo1.add(opcionC);
        opcionC.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        opcionC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcionCActionPerformed(evt);
            }
        });

        btnGrupo1.add(opcionD);
        opcionD.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        opcionD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcionDActionPerformed(evt);
            }
        });

        btnGrupo2.add(opcion0);
        opcion0.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        opcion0.setText("jRadioButton1");
        opcion0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcion0ActionPerformed(evt);
            }
        });

        btnGrupo2.add(opcion1A);
        opcion1A.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        opcion1A.setText("jRadioButton1");
        opcion1A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcion1AActionPerformed(evt);
            }
        });

        btnGrupo2.add(opcion1B);
        opcion1B.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        opcion1B.setText("jRadioButton1");
        opcion1B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcion1BActionPerformed(evt);
            }
        });

        btnGrupo2.add(opcion2A);
        opcion2A.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        opcion2A.setText("jRadioButton1");
        opcion2A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcion2AActionPerformed(evt);
            }
        });

        btnGrupo2.add(opcion2B);
        opcion2B.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        opcion2B.setText("jRadioButton1");
        opcion2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcion2BActionPerformed(evt);
            }
        });

        btnGrupo2.add(opcion3A);
        opcion3A.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        opcion3A.setText("jRadioButton1");
        opcion3A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcion3AActionPerformed(evt);
            }
        });

        btnGrupo2.add(opcion3B);
        opcion3B.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        opcion3B.setText("jRadioButton1");
        opcion3B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcion3BActionPerformed(evt);
            }
        });

        lbTitulo.setFont(new java.awt.Font("Verdana", 1, 36)); // NOI18N
        lbTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTitulo.setText("Test de Depresión");

        lbPregunta.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        lbPregunta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbPregunta.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        btnSiguiente.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSiguiente.setContentAreaFilled(false);
        btnSiguiente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSiguiente.setEnabled(false);
        btnSiguiente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSiguienteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSiguienteMouseExited(evt);
            }
        });
        btnSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteActionPerformed(evt);
            }
        });

        btnAnterior.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnAnterior.setContentAreaFilled(false);
        btnAnterior.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAnterior.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAnteriorMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAnteriorMouseExited(evt);
            }
        });
        btnAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnteriorActionPerformed(evt);
            }
        });

        lbProgreso.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbProgreso.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbProgreso.setText("Completado - 0%");

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lbPregunta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(panelLayout.createSequentialGroup()
                                .addComponent(btnAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lbProgreso, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(115, 115, 115)
                                .addComponent(btnSiguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(24, 24, 24))
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(opcion0)
                            .addComponent(lbTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 674, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelLayout.createSequentialGroup()
                                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(opcion1A)
                                    .addComponent(opcion3A)
                                    .addComponent(opcion2A)
                                    .addComponent(opcion2B)
                                    .addComponent(opcion1B)
                                    .addComponent(opcion3B))
                                .addGap(18, 18, 18)
                                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(opcionB)
                                    .addComponent(opcionC)
                                    .addComponent(opcionA)
                                    .addComponent(opcionD))))
                        .addGap(0, 12, Short.MAX_VALUE))))
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(lbTitulo)
                .addGap(45, 45, 45)
                .addComponent(lbPregunta, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(opcion0)
                .addGap(18, 18, 18)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addComponent(opcion1A)
                        .addGap(18, 18, 18)
                        .addComponent(opcion1B)
                        .addGap(18, 18, 18)
                        .addComponent(opcion2A)
                        .addGap(18, 18, 18)
                        .addComponent(opcion2B)
                        .addGap(13, 13, 13)
                        .addComponent(opcion3A)
                        .addGap(18, 18, 18)
                        .addComponent(opcion3B)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnSiguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbProgreso, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(46, 46, 46))
                    .addGroup(panelLayout.createSequentialGroup()
                        .addComponent(opcionA)
                        .addGap(26, 26, 26)
                        .addComponent(opcionB)
                        .addGap(26, 26, 26)
                        .addComponent(opcionC)
                        .addGap(26, 26, 26)
                        .addComponent(opcionD)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        getContentPane().add(panel, java.awt.BorderLayout.CENTER);

        menuArchivo.setText("Archivo");

        menuItemNuevo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_MASK));
        menuItemNuevo.setText("Comenzar de nuevo");
        menuItemNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemNuevoActionPerformed(evt);
            }
        });
        menuArchivo.add(menuItemNuevo);

        menuItemMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        menuItemMenu.setText("Volver al menu");
        menuItemMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemMenuActionPerformed(evt);
            }
        });
        menuArchivo.add(menuItemMenu);

        barra.add(menuArchivo);

        menuOpciones.setText("Opciones");

        menuItemRes1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_1, java.awt.event.InputEvent.CTRL_MASK));
        menuItemRes1.setText("Seleccionar opción 1");
        menuItemRes1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemRes1ActionPerformed(evt);
            }
        });
        menuOpciones.add(menuItemRes1);

        menuItemRes2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_2, java.awt.event.InputEvent.CTRL_MASK));
        menuItemRes2.setText("Seleccionar opción 2");
        menuItemRes2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemRes2ActionPerformed(evt);
            }
        });
        menuOpciones.add(menuItemRes2);

        menuItemRes3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_3, java.awt.event.InputEvent.CTRL_MASK));
        menuItemRes3.setText("Seleccionar opción 3");
        menuItemRes3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemRes3ActionPerformed(evt);
            }
        });
        menuOpciones.add(menuItemRes3);

        menuItemRes4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_4, java.awt.event.InputEvent.CTRL_MASK));
        menuItemRes4.setText("Seleccionar opción 4");
        menuItemRes4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemRes4ActionPerformed(evt);
            }
        });
        menuOpciones.add(menuItemRes4);

        menuItemRes5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_5, java.awt.event.InputEvent.CTRL_MASK));
        menuItemRes5.setText("Seleccionar opción 5");
        menuItemRes5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemRes5ActionPerformed(evt);
            }
        });
        menuOpciones.add(menuItemRes5);

        menuItemRes6.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_6, java.awt.event.InputEvent.CTRL_MASK));
        menuItemRes6.setText("Seleccionar opción 6");
        menuItemRes6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemRes6ActionPerformed(evt);
            }
        });
        menuOpciones.add(menuItemRes6);

        menuItemRes7.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_7, java.awt.event.InputEvent.CTRL_MASK));
        menuItemRes7.setText("Seleccionar opción 7");
        menuItemRes7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemRes7ActionPerformed(evt);
            }
        });
        menuOpciones.add(menuItemRes7);

        menuItemSiguiente.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_RIGHT, java.awt.event.InputEvent.CTRL_MASK));
        menuItemSiguiente.setText("Siguiente pregunta");
        menuItemSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemSiguienteActionPerformed(evt);
            }
        });
        menuOpciones.add(menuItemSiguiente);

        menuItemAnterior.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_LEFT, java.awt.event.InputEvent.CTRL_MASK));
        menuItemAnterior.setText("Anterior pregunta");
        menuItemAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemAnteriorActionPerformed(evt);
            }
        });
        menuOpciones.add(menuItemAnterior);

        barra.add(menuOpciones);

        setJMenuBar(barra);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteActionPerformed
        siguientePregunta();
    }//GEN-LAST:event_btnSiguienteActionPerformed

    private void btnAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnteriorActionPerformed
        anteriorPregunta();
    }//GEN-LAST:event_btnAnteriorActionPerformed

    private void menuItemNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemNuevoActionPerformed
        indicePregunta = 0;
        respuestas = new char[test.cantidadPreguntas];
        btnGrupo1.clearSelection();
        btnGrupo2.clearSelection();
        
        mostrarGrupo1(true);
        mostrarGrupo2(false);
        
        //Agregar primer pregunta
        lbPregunta.setText(test.preguntas[indicePregunta]);
        opcionA.setText(test.respuestas[indicePregunta][0]);
        opcionB.setText(test.respuestas[indicePregunta][1]);
        opcionC.setText(test.respuestas[indicePregunta][2]);
        opcionD.setText(test.respuestas[indicePregunta][3]);
        
        lbProgreso.setText("Completado - 0%");
        btnAnterior.setVisible(false);
        btnSiguiente.setEnabled(false);
        imagenBotonSiguiente();
    }//GEN-LAST:event_menuItemNuevoActionPerformed

    private void menuItemMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemMenuActionPerformed
        this.dispose();
        new MenuPrincipal().setVisible(true);
    }//GEN-LAST:event_menuItemMenuActionPerformed

    private void menuItemRes1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemRes1ActionPerformed
        if(opcionA.isVisible())
            opcionA.setSelected(true);
        if(opcion0.isVisible())
            opcion0.setSelected(true);
        opcionSeleccionada();
    }//GEN-LAST:event_menuItemRes1ActionPerformed

    private void menuItemRes2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemRes2ActionPerformed
        if(opcionB.isVisible())
            opcionB.setSelected(true);
        if(opcion1A.isVisible())
            opcion1A.setSelected(true);
        opcionSeleccionada();
    }//GEN-LAST:event_menuItemRes2ActionPerformed

    private void menuItemSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemSiguienteActionPerformed
        if(respuestas[indicePregunta] != 0){
            siguientePregunta();
        }else{
            acciones.dialogoSeleccionarOpcion(this);
        }
    }//GEN-LAST:event_menuItemSiguienteActionPerformed

    private void menuItemAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemAnteriorActionPerformed
        if(indicePregunta > 0)
            anteriorPregunta();
    }//GEN-LAST:event_menuItemAnteriorActionPerformed

    private void menuItemRes3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemRes3ActionPerformed
        if(opcionC.isVisible())
            opcionC.setSelected(true);
        if(opcion1B.isVisible())
            opcion1B.setSelected(true);
        opcionSeleccionada();
    }//GEN-LAST:event_menuItemRes3ActionPerformed

    private void menuItemRes4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemRes4ActionPerformed
        if(opcionD.isVisible())
            opcionD.setSelected(true);
        if(opcion2A.isVisible())
            opcion2A.setSelected(true);
        opcionSeleccionada();
    }//GEN-LAST:event_menuItemRes4ActionPerformed

    private void menuItemRes5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemRes5ActionPerformed
        if(opcion2B.isVisible()){
            opcion2B.setSelected(true);
            opcionSeleccionada();
        }
    }//GEN-LAST:event_menuItemRes5ActionPerformed

    private void menuItemRes6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemRes6ActionPerformed
        if(opcion3A.isVisible()){
            opcion3A.setSelected(true);
            opcionSeleccionada();
        }
    }//GEN-LAST:event_menuItemRes6ActionPerformed

    private void menuItemRes7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemRes7ActionPerformed
        if(opcion3B.isVisible()){
            opcion3B.setSelected(true);
            opcionSeleccionada();
        }
    }//GEN-LAST:event_menuItemRes7ActionPerformed

    private void opcionAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcionAActionPerformed
        opcionSeleccionada();
    }//GEN-LAST:event_opcionAActionPerformed

    private void opcionBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcionBActionPerformed
        opcionSeleccionada();
    }//GEN-LAST:event_opcionBActionPerformed

    private void opcionCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcionCActionPerformed
        opcionSeleccionada();
    }//GEN-LAST:event_opcionCActionPerformed

    private void opcionDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcionDActionPerformed
        opcionSeleccionada();
    }//GEN-LAST:event_opcionDActionPerformed

    private void opcion0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcion0ActionPerformed
        opcionSeleccionada();
    }//GEN-LAST:event_opcion0ActionPerformed

    private void opcion1AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcion1AActionPerformed
        opcionSeleccionada();
    }//GEN-LAST:event_opcion1AActionPerformed

    private void opcion1BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcion1BActionPerformed
        opcionSeleccionada();
    }//GEN-LAST:event_opcion1BActionPerformed

    private void opcion2AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcion2AActionPerformed
        opcionSeleccionada();
    }//GEN-LAST:event_opcion2AActionPerformed

    private void opcion2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcion2BActionPerformed
        opcionSeleccionada();
    }//GEN-LAST:event_opcion2BActionPerformed

    private void opcion3AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcion3AActionPerformed
        opcionSeleccionada();
    }//GEN-LAST:event_opcion3AActionPerformed

    private void opcion3BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcion3BActionPerformed
        opcionSeleccionada();
    }//GEN-LAST:event_opcion3BActionPerformed

    private void btnSiguienteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSiguienteMouseEntered
        if(indicePregunta == test.cantidadPreguntas - 1)
            imagenBotonFinalizarGrande();
        else
            imagenBotonSiguienteGrande();
    }//GEN-LAST:event_btnSiguienteMouseEntered

    private void btnSiguienteMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSiguienteMouseExited
        if(indicePregunta == test.cantidadPreguntas - 1)
            imagenBotonFinalizar();
        else
            imagenBotonSiguiente();
    }//GEN-LAST:event_btnSiguienteMouseExited

    private void btnAnteriorMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAnteriorMouseEntered
        imagenBotonAnteriorGrande();
    }//GEN-LAST:event_btnAnteriorMouseEntered

    private void btnAnteriorMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAnteriorMouseExited
        imagenBotonAnterior();
    }//GEN-LAST:event_btnAnteriorMouseExited

    private void opcionSeleccionada(){
        btnSiguiente.setEnabled(true);
        guardarRespuesta();
        actualizarProgreso();
    }
    
    private void siguientePregunta(){
        
        if(indicePregunta == test.cantidadPreguntas - 1){
            int respuesta = acciones.dialogoTerminarTest(this);
            if(respuesta == 0){
                this.dispose();
                new TestDepresionResultados(respuestas).setVisible(true);
            }
        }else{
            indicePregunta++;

            if(indicePregunta == 15 || indicePregunta == 17){
                mostrarGrupo1(false);
                mostrarGrupo2(true);
                lbPregunta.setText(test.preguntas[indicePregunta]);
                opcion0.setText(test.respuestas[indicePregunta][0]);
                opcion1A.setText(test.respuestas[indicePregunta][1]);
                opcion1B.setText(test.respuestas[indicePregunta][2]);
                opcion2A.setText(test.respuestas[indicePregunta][3]);
                opcion2B.setText(test.respuestas[indicePregunta][4]);
                opcion3A.setText(test.respuestas[indicePregunta][5]);
                opcion3B.setText(test.respuestas[indicePregunta][6]);
            }else{
                mostrarGrupo1(true);
                mostrarGrupo2(false);
                lbPregunta.setText(test.preguntas[indicePregunta]);
                opcionA.setText(test.respuestas[indicePregunta][0]);
                opcionB.setText(test.respuestas[indicePregunta][1]);
                opcionC.setText(test.respuestas[indicePregunta][2]);
                opcionD.setText(test.respuestas[indicePregunta][3]);
            }

            llenarRespuesta();

            if(indicePregunta == test.cantidadPreguntas - 1)
                imagenBotonFinalizar();
        }

        if(indicePregunta > 0){
            btnAnterior.setVisible(true);
        }
    }
    
    private void anteriorPregunta(){
        
        guardarRespuesta();
        indicePregunta--;
        if(indicePregunta == 15 || indicePregunta == 17){
            mostrarGrupo1(false);
            mostrarGrupo2(true);
        }else{
            mostrarGrupo1(true);
            mostrarGrupo2(false);
        }
        llenarRespuesta();
        
        if(indicePregunta == 0){
            btnAnterior.setVisible(false);
        }
        
        if(indicePregunta < test.cantidadPreguntas - 1){
            imagenBotonSiguiente();
        }
        
        if(indicePregunta < test.cantidadPreguntas){
            btnSiguiente.setVisible(true);
        }
        
        if(indicePregunta == 15 || indicePregunta == 17){
            lbPregunta.setText(test.preguntas[indicePregunta]);
            opcion0.setText(test.respuestas[indicePregunta][0]);
            opcion1A.setText(test.respuestas[indicePregunta][1]);
            opcion1B.setText(test.respuestas[indicePregunta][2]);
            opcion2A.setText(test.respuestas[indicePregunta][3]);
            opcion2B.setText(test.respuestas[indicePregunta][4]);
            opcion3A.setText(test.respuestas[indicePregunta][5]);
            opcion3B.setText(test.respuestas[indicePregunta][6]);
        }else{
            lbPregunta.setText(test.preguntas[indicePregunta]);
            opcionA.setText(test.respuestas[indicePregunta][0]);
            opcionB.setText(test.respuestas[indicePregunta][1]);
            opcionC.setText(test.respuestas[indicePregunta][2]);
            opcionD.setText(test.respuestas[indicePregunta][3]);
        }
    }
    
    private void guardarRespuesta(){
        if(opcionA.isVisible())
            respuestas[indicePregunta] = acciones.obtenerRespuesta(opcionA, opcionB, opcionC, opcionD);
        else
            respuestas[indicePregunta] = acciones.obtenerRespuesta(opcion0, opcion1A, opcion1B, opcion2A, opcion2B, opcion3A, opcion3B);
    }
    
    private void llenarRespuesta(){
        
        btnGrupo1.clearSelection();
        btnGrupo2.clearSelection();
        
        char respuesta = respuestas[indicePregunta];
        
        switch(respuesta){
            case 'A':
                if(opcionA.isVisible())
                    opcionA.setSelected(true);
                else
                    opcion0.setSelected(true);
                break;
            case 'B':
                if(opcionB.isVisible())
                    opcionB.setSelected(true);
                else
                    opcion1A.setSelected(true);
                break;
            case 'C':
                if(opcionC.isVisible())
                    opcionC.setSelected(true);
                else
                    opcion1B.setSelected(true);
                break;
            case 'D':
                if(opcionD.isVisible())
                    opcionD.setSelected(true);
                else
                    opcion2A.setSelected(true);
                break;
            case 'E':
                opcion2B.setSelected(true);
                break;
            case 'F':
                opcion3A.setSelected(true);
                break;
            case 'G':
                opcion3B.setSelected(true);
                break;
        }
        
        if(respuesta != 0)
            btnSiguiente.setEnabled(true);
        else
            btnSiguiente.setEnabled(false);
    }
    
    private void actualizarProgreso(){
        int contestadas = 0;
        int completado = 0;
        for(int i = 0; i < test.cantidadPreguntas; i++){
            if(respuestas[i] != 0)
                contestadas++;
        }
        
        completado = contestadas * 100 / test.cantidadPreguntas; 
        
        lbProgreso.setText("Completado - " + completado + "%");
    }
    
    private void mostrarGrupo1(boolean esconder){
        opcionA.setVisible(esconder);
        opcionB.setVisible(esconder);
        opcionC.setVisible(esconder);
        opcionD.setVisible(esconder);
    }
    
    private void mostrarGrupo2(boolean esconder){
        opcion0.setVisible(esconder);
        opcion1A.setVisible(esconder);
        opcion1B.setVisible(esconder);
        opcion2A.setVisible(esconder);
        opcion2B.setVisible(esconder);
        opcion3A.setVisible(esconder);
        opcion3B.setVisible(esconder);
    }
    
    private void imagenBotonSiguiente(){
        int ancho = btnSiguiente.getSize().width;
        int alto = btnSiguiente.getSize().height - 10;
        ImageIcon imagenSiguiente = new ImageIcon(acciones.rutaImagenes + "siguiente.png");
        
        btnSiguiente.setIcon(acciones.ajustarImagen(imagenSiguiente, ancho, alto));
    }
    
     private void imagenBotonSiguienteGrande(){
        if(respuestas[indicePregunta] != 0){
            int ancho = btnSiguiente.getSize().width;
            int alto = btnSiguiente.getSize().height + 10;
            ImageIcon imagenSiguiente = new ImageIcon(acciones.rutaImagenes + "siguiente.png");

            btnSiguiente.setIcon(acciones.ajustarImagen(imagenSiguiente, ancho, alto));
        }
    }
    
    private void imagenBotonAnterior(){
        int ancho = btnAnterior.getSize().width;
        int alto = btnAnterior.getSize().height - 10;
        ImageIcon imagenAnterior = new ImageIcon(acciones.rutaImagenes + "anterior.png");
        
        btnAnterior.setIcon(acciones.ajustarImagen(imagenAnterior, ancho, alto));
    }
    
    private void imagenBotonAnteriorGrande(){
        int ancho = btnAnterior.getSize().width;
        int alto = btnAnterior.getSize().height + 10;
        ImageIcon imagenAnterior = new ImageIcon(acciones.rutaImagenes + "anterior.png");
        
        btnAnterior.setIcon(acciones.ajustarImagen(imagenAnterior, ancho, alto));
    }
    
    private void imagenBotonFinalizar(){
        int ancho = btnSiguiente.getSize().width;
        int alto = btnSiguiente.getSize().height - 10;
        ImageIcon imagenFinalizar = new ImageIcon(acciones.rutaImagenes + "finalizar.png");
        
        btnSiguiente.setIcon(acciones.ajustarImagen(imagenFinalizar, ancho, alto));
    }
    
    private void imagenBotonFinalizarGrande(){
        if(respuestas[indicePregunta] != 0){
            int ancho = btnSiguiente.getSize().width;
            int alto = btnSiguiente.getSize().height + 10;
            ImageIcon imagenFinalizar = new ImageIcon(acciones.rutaImagenes + "finalizar.png");

            btnSiguiente.setIcon(acciones.ajustarImagen(imagenFinalizar, ancho, alto));
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar barra;
    private javax.swing.JButton btnAnterior;
    private javax.swing.ButtonGroup btnGrupo1;
    private javax.swing.ButtonGroup btnGrupo2;
    private javax.swing.JButton btnSiguiente;
    private javax.swing.JLabel lbPregunta;
    private javax.swing.JLabel lbProgreso;
    private javax.swing.JLabel lbTitulo;
    private javax.swing.JMenu menuArchivo;
    private javax.swing.JMenuItem menuItemAnterior;
    private javax.swing.JMenuItem menuItemMenu;
    private javax.swing.JMenuItem menuItemNuevo;
    private javax.swing.JMenuItem menuItemRes1;
    private javax.swing.JMenuItem menuItemRes2;
    private javax.swing.JMenuItem menuItemRes3;
    private javax.swing.JMenuItem menuItemRes4;
    private javax.swing.JMenuItem menuItemRes5;
    private javax.swing.JMenuItem menuItemRes6;
    private javax.swing.JMenuItem menuItemRes7;
    private javax.swing.JMenuItem menuItemSiguiente;
    private javax.swing.JMenu menuOpciones;
    private javax.swing.JRadioButton opcion0;
    private javax.swing.JRadioButton opcion1A;
    private javax.swing.JRadioButton opcion1B;
    private javax.swing.JRadioButton opcion2A;
    private javax.swing.JRadioButton opcion2B;
    private javax.swing.JRadioButton opcion3A;
    private javax.swing.JRadioButton opcion3B;
    private javax.swing.JRadioButton opcionA;
    private javax.swing.JRadioButton opcionB;
    private javax.swing.JRadioButton opcionC;
    private javax.swing.JRadioButton opcionD;
    private javax.swing.JPanel panel;
    // End of variables declaration//GEN-END:variables
}
