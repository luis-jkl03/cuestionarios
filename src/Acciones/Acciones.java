package Acciones;

import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

public class Acciones {
    
    public String rutaImagenes = "src/imagenes/";
    
    public char obtenerRespuesta(JRadioButton radiobtn1, JRadioButton radiobtn2){
        char respuesta = 0;
        if(radiobtn1.isSelected())
            respuesta = 'A';
        else if(radiobtn2.isSelected())
            respuesta = 'B';
        
        return respuesta;
    }
    
    public char obtenerRespuesta(JRadioButton radiobtn1, JRadioButton radiobtn2, JRadioButton radiobtn3){
        char respuesta = 0;
        if(radiobtn1.isSelected())
            respuesta = 'A';
        else if(radiobtn2.isSelected())
            respuesta = 'B';
        else if(radiobtn3.isSelected())
            respuesta = 'C';
        
        return respuesta;
    }
    
    public char obtenerRespuesta(JRadioButton radiobtn1, JRadioButton radiobtn2, JRadioButton radiobtn3, 
            JRadioButton radiobtn4){
        char respuesta = 0;
        if(radiobtn1.isSelected())
            respuesta = 'A';
        else if(radiobtn2.isSelected())
            respuesta = 'B';
        else if(radiobtn3.isSelected())
            respuesta = 'C';
        else if(radiobtn4.isSelected())
            respuesta = 'D';
        
        return respuesta;
    }
    
    public char obtenerRespuesta(JRadioButton radiobtn1, JRadioButton radiobtn2, JRadioButton radiobtn3, 
            JRadioButton radiobtn4, JRadioButton radiobtn5, JRadioButton radiobtn6, JRadioButton radiobtn7){
        char respuesta = 0;
        if(radiobtn1.isSelected())
            respuesta = 'A';
        else if(radiobtn2.isSelected())
            respuesta = 'B';
        else if(radiobtn3.isSelected())
            respuesta = 'C';
        else if(radiobtn4.isSelected())
            respuesta = 'D';
        else if(radiobtn5.isSelected())
            respuesta = 'E';
        else if(radiobtn6.isSelected())
            respuesta = 'F';
        else if(radiobtn7.isSelected())
            respuesta = 'G';
        
        return respuesta;
    }
    
    public void dialogoSeleccionarOpcion(JFrame frame){
        JOptionPane.showMessageDialog(frame, "Se debe seleccionar una opción para continuar");
    }
    
    public int dialogoTerminarTest(JFrame frame) {
        int respuesta = JOptionPane.showConfirmDialog(frame, "Esta a punto de finalizar el test, desea continuar?", 
                "Finalizar Test", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        return respuesta;
    }
    
    public ImageIcon ajustarImagen(ImageIcon imagen, int ancho, int alto){
        Image img = imagen.getImage();
        ImageIcon nuevaImagen = new ImageIcon(img.getScaledInstance(ancho, alto, Image.SCALE_SMOOTH));
        
        return nuevaImagen;
        
    }
}
